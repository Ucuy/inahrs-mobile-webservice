-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2016 at 05:36 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inahrs_mobile`
--

-- --------------------------------------------------------

--
-- Table structure for table `downloadable`
--

CREATE TABLE `downloadable` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `link` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloadable`
--

INSERT INTO `downloadable` (`id`, `name`, `link`) VALUES
(1, 'Gambar Jimin', 'http://data.whicdn.com/images/139588684/large.jpg'),
(2, 'Paper Social Media', 'www.tandf.co.uk/journals/access/white-paper-infographic-social-media-future.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT 'Whether this is a workshop (1) or symposium (2).',
  `description` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table refers to both workshop and symposium events.';

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `type`, `description`, `image`) VALUES
(1, 1, 'Workshop satu', '');

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE `hotel` (
  `hotel_id` int(11) NOT NULL,
  `hotel_name` text NOT NULL,
  `hotel_image` text NOT NULL,
  `hotel_price` text NOT NULL,
  `hotel_detail` text NOT NULL,
  `hotel_lat` float(9,5) NOT NULL,
  `hotel_lng` float(9,5) NOT NULL,
  `hotel_alamat` text NOT NULL,
  `hotel_telepon` text NOT NULL,
  `hotel_distance` float(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`hotel_id`, `hotel_name`, `hotel_image`, `hotel_price`, `hotel_detail`, `hotel_lat`, `hotel_lng`, `hotel_alamat`, `hotel_telepon`, `hotel_distance`) VALUES
(1, 'Hotel Rasuna Icon', 'http://pix2.agoda.net/hotelImages/281/281402/281402_14022612270018472377.jpg?s=1100x825&ar=16x9', '436000', '', -6.22082, 106.82801, 'Jalan Karet Pedurenan No.3, Karet Kuningan, Setiabudi, Daerah Khusus Ibukota Jakarta', '(021) 5201642', 1.72);

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `speaker`
--

CREATE TABLE `speaker` (
  `speaker_id` int(11) NOT NULL,
  `speaker_name` text NOT NULL,
  `speaker_description` text NOT NULL,
  `speaker_nationality` text NOT NULL,
  `speaker_image` text NOT NULL,
  `speaker_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `speaker`
--

INSERT INTO `speaker` (`speaker_id`, `speaker_name`, `speaker_description`, `speaker_nationality`, `speaker_image`, `speaker_title`) VALUES
(1, 'Febby Haryono', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Indonesia', 'http://tangselpos.co.id/wp-content/uploads/2015/08/Febby-Haryono.jpg', 'Dokter Umum');

-- --------------------------------------------------------

--
-- Table structure for table `sponsor`
--

CREATE TABLE `sponsor` (
  `sponsor_id` int(11) NOT NULL,
  `sponsor_name` text NOT NULL,
  `sponsor_image` text NOT NULL,
  `sponsor_description` text NOT NULL,
  `sponsor_website` text NOT NULL,
  `sponsor_address` text NOT NULL,
  `sponsor_phone` text NOT NULL,
  `sponsor_email` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sponsor`
--

INSERT INTO `sponsor` (`sponsor_id`, `sponsor_name`, `sponsor_image`, `sponsor_description`, `sponsor_website`, `sponsor_address`, `sponsor_phone`, `sponsor_email`) VALUES
(1, 'Wardah', 'http://logos.textgiraffe.com/logos/logo-name/Wardah-designstyle-love-heart-m.png', 'Wardah adalah kosmetik lorem ipsum.', 'Mr. Wardah, S.Pd.', '', '', ''),
(2, 'Asus', '', '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `downloadable`
--
ALTER TABLE `downloadable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
  ADD PRIMARY KEY (`hotel_id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `speaker`
--
ALTER TABLE `speaker`
  ADD PRIMARY KEY (`speaker_id`);

--
-- Indexes for table `sponsor`
--
ALTER TABLE `sponsor`
  ADD PRIMARY KEY (`sponsor_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `downloadable`
--
ALTER TABLE `downloadable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
  MODIFY `hotel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `speaker`
--
ALTER TABLE `speaker`
  MODIFY `speaker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sponsor`
--
ALTER TABLE `sponsor`
  MODIFY `sponsor_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
