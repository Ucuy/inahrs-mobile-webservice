<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return 'Homepage!';
});

/*
|--------------------------------------------------------------------------
| Webservices routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/mobile-login', function () {

    // $jsonResult = array();

    // Cari ID participant berdasar email yang diberikan
    // Harap ingat bahwa participant beda dengan registrant

    // HATI-HATI, DUA PARTISIPAN YANG BERBEDA!
    // UNTUK SAAT INI
    $participantId =
        DB::connection('inahrs_website')
            ->table('event_registrant_participant')
            ->select('id', 'fullname')
            ->where('email', Input::get('email'))
            ->orderBy('id', 'DESC')
            ->take(1)// AMBIL YANG TERBARU
            ->pluck('id');

    $profile =
        DB::connection('inahrs_website')
            ->table('event_registrant_participant')
            ->select(
                'id as participant_id',
                'registrant_id as registrant_id',
                'fullname')
            ->where('id', $participantId)
            ->first();

    $workshopIds =
        DB::connection('inahrs_website')
            ->table('event_participant_workshop')
            ->select(
                'event_workshop_id AS workshop_id'
            )
            ->where('event_participant_id', $participantId)
            ->pluck('workshop_id');


    $symposiumSpecialization =
        DB::connection('inahrs_website')
            ->table('event_participant_symposium')
            ->select(
                'event_symposium_id AS symposium_id'
            )
            ->where('event_participant_id', $participantId)
            ->pluck('symposium_id');

    $jsonResult['profile'] = $profile;
    $jsonResult['workshopIds'] = $workshopIds;
    $jsonResult['symposiumSpecialization'] = $symposiumSpecialization;

    return $jsonResult;

});

/* PROGRAM ROUTES */
// API
Route::get('program/all', 'ProgramController@getProgramList');
Route::get('program/{id}', 'ProgramController@getProgram');
// CMS
Route::get('cms/program', 'CmsController@program'); // returns programs's view page
Route::post('cms/updateProgram', 'CmsController@updateProgram'); // update program information
Route::post('cms/addProgram', 'CmsController@addProgram');
Route::post('cms/deleteProgram/{id}', 'CmsController@deleteProgram');

/* SYMPOSIUM ROUTES */
// API
Route::get('symposium/specialization', 'SymposiumController@getSpecializationList');
Route::get('symposium/specialization/{id}', 'SymposiumController@getSpecialization');
Route::get('symposium/getAllForSpec/{id}', 'SymposiumController@getAllForSpec');
Route::get('symposium/{id}', 'SymposiumController@getSymposium');

// CMS
Route::get('cms/symposium', 'CmsController@symposium'); // returns symposium's view page
Route::post('cms/updateSymposium', 'CmsController@updateSymposium'); // update symposium information
Route::post('cms/addSymposiumForSpec/{id}', 'CmsController@addSymposiumForSpec');
Route::post('cms/deleteSymposium/{id}', 'CmsController@deleteSymposium');

/* WORKSHOP ROUTES */
//API
Route::get('workshop/all', 'WorkshopController@getWorkshopList');
Route::get('workshop/{id}', 'WorkshopController@getWorkshop');

// CMS
Route::get('cms/workshop', 'CmsController@workshop');
Route::post('cms/updateWorkshop', 'CmsController@updateWorkshop');
Route::post('cms/addWorkshop', 'CmsController@addWorkshop');
Route::post('cms/deleteWorkshop/{id}', 'CmsController@deleteWorkshop');


// QUESTION ROUTES
// FOR WORKSHOP
Route::get('cms/workshopQuestion', 'CmsController@workshopQuestion');
Route::get('question/workshop/get/{id}', 'QuestionController@getWorkshopQuestionList');

// FOR SYMPO
Route::get('cms/sympoQuestion', 'CmsController@sympoQuestion');
Route::get('question/sympo/get/{id}', 'QuestionController@getSympoQuestionList');

// API
Route::get('question/workshop/{id}', 'QuestionController@addWorkshopQuestion');
Route::get('question/symposium/{id}', 'QuestionController@addSympoQuestion');


/* SPEAKER ROUTES */
// API
Route::get('speaker/all', 'SpeakerController@getSpeakerList');
Route::get('speaker/{id}', 'SpeakerController@getSpeaker');

// CMS
Route::get('cms/speaker', 'CmsController@speaker');
Route::post('cms/updateSpeaker', 'CmsController@updateSpeaker');
Route::post('cms/addSpeaker', 'CmsController@addSpeaker');
Route::post('cms/deleteSpeaker/{id}', 'CmsController@deleteSpeaker');


/* SPONSOR ROUTES */
// API
Route::get('sponsor/all', 'SponsorController@getSponsorList');
Route::get('sponsor/{id}', 'SponsorController@getSponsor');
// CMS
Route::get('cms/sponsor', 'CmsController@sponsor'); // returns sponsor's view page
Route::post('cms/updateSponsor', 'CmsController@updateSponsor'); // update sponsor information
Route::post('cms/addSponsor', 'CmsController@addSponsor'); // addSponsor
Route::post('cms/deleteSponsor/{id}', 'CmsController@deleteSponsor'); // deleteSponsor

/* HOTEL ROUTES */
// API
Route::get('hotel/all', 'HotelController@getHotelList');
Route::get('hotel/{id}', 'HotelController@getHotel');
// CMS
Route::get('cms/hotel', 'CmsController@hotel'); // returns hotel's view page
Route::post('cms/updateHotel', 'CmsController@updateHotel'); // update hotel information
Route::post('cms/addHotel', 'CmsController@addHotel'); // addHotel
Route::post('cms/deleteHotel/{id}', 'CmsController@deleteHotel'); // addHotel

/*
|--------------------------------------------------------------------------
| Cms Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return redirect('/cms');
});
Route::get('cms', 'CmsController@welcome');
Route::get('cms/login', 'CmsController@getLogin');
Route::post('cms/login', 'CmsController@postLogin');
Route::get('cms/logout', 'CmsController@getLogout');
Route::get('cms/dashboard', 'CmsController@dashboard');
