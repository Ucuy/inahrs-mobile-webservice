<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use DB;

class SpeakerController extends Controller
{
    //
    public function getSpeakerList()
    {
        $speakers = DB::table('speaker')->select(
            'speaker_id as id',
            'speaker_image as image',
            'speaker_name as name',
            'speaker_title as title',
            'speaker_nationality as nationality'
        )->get();
        return $speakers;
    }

    public function getSpeaker($id)
    {
        $speaker = DB::table('speaker')->where('speaker_id', $id)->get();
        return $speaker;
    }
}
