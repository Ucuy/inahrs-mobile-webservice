<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

class WorkshopController extends Controller
{

    public function getWorkshopList()
    {
        $workshops = DB::table('workshop')
            ->select(
                'workshop_id as id',
                'workshop_origin_workshop_id as origin_id',
                'workshop_name as name',
                'workshop_image as image',
                'workshop_description as description',
                'workshop_date as date',
                'workshop_start_time as start_time',
                'workshop_end_time as end_time',
                'workshop_venue as venue'
            )
            ->get();

        return $workshops;
    }

    public function getWorkshop($workshopId)
    {
        $hotel = DB::table('workshop')->where('workshop_id', $workshopId)->get();
        return $hotel;
    }
}