<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class QuestionController extends Controller
{
    public function getEventIds()
    {
        $eventIds = DB::table('question')
            ->select(
                'question_event_id as event_id',
                'event_name'
            )
            ->join('event', 'question_event_id', '=', 'event_id')
            ->groupBy('question_event_id')
            ->get('event_id');

        return $eventIds;
    }

    public function getWorkshopQuestionList($workshopEventId)
    {
        $questions = DB::table('workshop_question')
            ->where('question_workshop_id', $workshopEventId)
            ->get();
        return $questions;
    }

    public function getSympoQuestionList($symposiumId)
    {
        $questions = DB::table('symposium_question')
            ->where('question_symposium_id', $symposiumId)
            ->get();
        return $questions;
    }

    public function addWorkshopQuestion($workshopId)
    {
        try {
            DB::table('workshop_question')
                ->insert([
                    'question_workshop_id' => $workshopId,
                    'question_asker' => Input::get('asker'),
                    'question_content' => Input::get('content'),
                    'question_time' => date("Y-m-d H:i:s")
                ]);

            return "Question asked!";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }

    public function addSympoQuestion($sympoId)
    {
        try {
            DB::table('symposium_question')
                ->insert([
                    'question_symposium_id' => $sympoId,
                    'question_asker' => Input::get('asker'),
                    'question_content' => Input::get('content'),
                    'question_time' => date("Y-m-d H:i:s")
                ]);

            return "Question asked!";
        } catch (\Exception $anything) {
            return "Failed: " . $anything;
        }
    }


}
