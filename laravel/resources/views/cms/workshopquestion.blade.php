@extends('cms.master')

@section('content')
    <div class="col-md-4">
        <h3 class="page-header">Workshop List</h3>
        <p id="programList">Loading...</p>
    </div>
    <div class="col-md-8">
        <h3 class="page-header">Workshop Questions</h3>
        <p id="questionList">Loading...</p>
    </div>
@endsection

@section('script')
    <script>

        $('#workshopQuestionSidebarItem').addClass("active");

        // Fetch and show list of workshops
        $.getJSON("../workshop/all", function (workshop) {
            console.log("Getting all workshops...");
            var workshopListHTMLBody = "";
            $.each(workshop, function (index, element) {
                console.log("WS id: " + index + ", name " + element.name + ", eventId = " + element.id);
                workshopListHTMLBody += "<li>" +
                        "<a href='#' onclick='reloadWorkshopQuestion(" + this['id'] + ")'>" + this['name'] + "</a>" +
                        "</li>";
            });
            $("#programList").html("<ol>" + workshopListHTMLBody + "</ol>");
            reloadWorkshopQuestion(1); // hardcoded wkwkwkwk
        });

        function reloadWorkshopQuestion(id) {
            console.log("Reloading WORKSHOP question for #" + id);
            $.getJSON("../question/workshop/get/" + id, function (question) {

                var workshopListHTMLBody = "";
                $.each(question, function (index, item) {
                    console.log("QSTIN id: " + index + ", name " + item.question_asker + ", eventId = " + item.id);
                    workshopListHTMLBody += "<li>" +
                            "(" + this.question_asker + ", " + this.question_time + ") " + this.question_content +
                            "</li>";
                });
                $("#questionList").html("<ol>" + workshopListHTMLBody + "</ol>");
            });
        }

    </script>
@endsection