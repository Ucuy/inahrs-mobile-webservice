@extends('cms.master')

@section('content')
    <div class="col-md-4">
        <h3 class="page-header">Workshop List</h3>
        <p id="programList">Loading...</p>
    </div>
    <div class="col-md-8">
        <h3 class="page-header">Workshop Detail</h3>

        {{ Form::open(['action' => 'CmsController@updateWorkshop', 'files' => 'true', 'id' => 'workshopForm', 'class' => 'form-horizontal']) }}
        <div class="form-group">
            <label class="sr-only col-sm-2 control-label">ID</label>
        </div>

        {{-- WORKSHOP ID --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">ID</label>
            <div class="col-sm-10">
                <input class="form-control" id="workshopId" name="workshopId">
            </div>
        </div>

        {{-- WORKSHOP NAME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input class="form-control" id="workshopName" name="workshopName">
            </div>
        </div>

        {{-- WORKSHOP IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-10">
                <input class="form-control" id="workshopImage" name="workshopImage">
            </div>
        </div>

        {{-- UPLOAD IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">{!! Form::label('upload', 'Upload') !!}</label>
            <div class="col-sm-10">
                {!! Form::file('upload') !!}
            </div>
        </div>

        {{-- WORKSHOP DESCRIPTION --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <input class="form-control" id="workshopDescription" name="workshopDescription">
            </div>
        </div>

        {{-- WORKSHOP DATE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Date</label>
            <div class="col-sm-10">
                <input class="form-control" id="workshopDate" name="workshopDate">
            </div>
        </div>

        {{-- WORKSHOP START TIME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Start Time</label>
            <div class="col-sm-10">
                <input class="form-control" id="workshopStartTime" name="workshopStartTime">
            </div>
        </div>

        {{-- WORKSHOP END TIME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">End Time</label>
            <div class="col-sm-10">
                <input class="form-control" id="workshopEndTime" name="workshopEndTime">
            </div>
        </div>

        {{-- WORKSHOP VENUE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Venue</label>
            <div class="col-sm-10">
                <input class="form-control" id="workshopVenue" name="workshopVenue">
            </div>
        </div>

        {{-- BUTTON TO SEND THE FORM AND UPDATE WORKSHOP --}}
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Update Workshop Detail</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script>

        $('#workshopSidebarItem').addClass("active");

        // Fetch and show list of workshops
        $.getJSON("../workshop/all", function (workshop) {
            console.log("Getting all workshops...");
            var workshopListHTMLBody = "";
            $.each(workshop, function (index, element) {
                console.log("SYMPOSIUM id: " + index + ", name " + element.name + ", eventId = " + element.id);
                workshopListHTMLBody += "<li>" +
                        "<a href='#' onclick='reloadWorkshopDetail(" + this['id'] + ")'>" + this['name'] + "</a> " +
                        "</li>";
            });
            $("#programList").html("<ol>" + workshopListHTMLBody + "</ol>");
            reloadWorkshopDetail(1); // hardcoded wkwkwkwk
        });

        function reloadWorkshopDetail(id) {
            console.log("Reloading WORKSHOP detail for #" + id);
            $.getJSON("../workshop/" + id, function (workshop) {

                console.log(workshop[0]); // get the zeroth hotel

                // iterate through JSONObject's keys
                $.each(workshop[0], function (key, value) {
                    // console.log("key: " + key + " value:" + value);
                    switch (key) {
                        case "workshop_id":
                            $("#workshopId").text(value).val(value);
                            return true;
                        case "workshop_name":
                            $("#workshopName").text(value).val(value);
                            return true;
                        case "workshop_image":
                            $("#workshopImage").text(value).val(value);
                            return true;
                        case "workshop_description":
                            $("#workshopDescription").text(value).val(value);
                            return true;
                        case "workshop_date":
                            $("#workshopDate").text(value).val(value);
                            return true;
                        case "workshop_start_time":
                            $("#workshopStartTime").text(value).val(value);
                            return true;
                        case "workshop_end_time":
                            $("#workshopEndTime").text(value).val(value);
                            return true;
                        case "workshop_venue":
                            $("#workshopVenue").text(value).val(value);
                            return true;
                    }
                });
            });
        }

        // AJAX form posting
        var form = $('#workshopForm');
        form.submit(function (ev) {

            // When submitting a form,
            // send the AJAX to the updateProgram
            // then make the form prevent default action
            console.log("Form sent!");
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: new FormData(form),
                success: function (data) {
                    alert(data);
                }
            });

            ev.preventDefault();
        });

    </script>
@endsection