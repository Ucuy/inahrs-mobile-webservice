@extends('cms.master')

@section('content')
    <div class="col-md-4">
        <h3 class="page-header">Hotel List</h3>
        <p id="hotelList">Loading...</p>
        <button type="button" class="btn btn-primary" id="addHotel">Add Hotel</button>
    </div>
    <div class="col-md-8">
        <h3 class="page-header">Hotel Detail</h3>

        {{ Form::open(['action' => 'CmsController@updateHotel', 'files' => 'true', 'id' => 'hotelForm', 'class' => 'form-horizontal']) }}

        <div class="form-group">
            <label class="sr-only col-sm-2 control-label">ID</label>
        </div>

        {{-- HOTEL ID --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">ID</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelId" name="hotelId">
            </div>
        </div>

        {{-- HOTEL NAME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelName" name="hotelName">
            </div>
        </div>

        {{-- HOTEL IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelImage" name="hotelImage">
            </div>
        </div>

        {{-- UPLOAD IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">{!! Form::label('upload', 'Upload') !!}</label>
            <div class="col-sm-10">
                {!! Form::file('upload') !!}
            </div>
        </div>

        {{-- HOTEL PRICE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Price</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelPrice" name="hotelPrice">
            </div>
        </div>

        {{-- HOTEL DETAIL --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Detail</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelDetail" name="hotelDetail">
            </div>
        </div>

        {{-- HOTEL LAT --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Latitude</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelLat" name="hotelLat">
            </div>
        </div>

        {{-- HOTEL LONGITUDE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Longitude</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelLng" name="hotelLng">
            </div>
        </div>

        {{-- HOTEL ALAMAT --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Alamat</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelAlamat" name="hotelAlamat">
            </div>
        </div>

        {{-- HOTEL TELEPON --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Telepon</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelTelepon" name="hotelTelepon">
            </div>
        </div>

        {{-- HOTEL DISTANCE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Distance</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelDistance" name="hotelDistance">
            </div>
        </div>

        {{-- HOTEL WEBSITE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Website</label>
            <div class="col-sm-10">
                <input class="form-control" id="hotelWebsite" name="hotelWebsite">
            </div>
        </div>

        {{-- BUTTON TO SEND THE FORM AND UPDATE HOTEL --}}
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Update Hotel Detail</button>
            </div>
        </div>

        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script>

        // Highlight the hotel in sidebar
        $('#hotelSidebarItem').addClass("active");

        // Fetch and show list of hotels
        $.getJSON("../hotel/all", function (hotel) {
            var hotelListHTMLBody = "";
            $.each(hotel, function (index, element) {
                console.log("Hotel id: " + index + ", name " + element.name);
                hotelListHTMLBody +=
                        "<li>" +
                        "<a href='#' onclick='reloadHotelDetail(" + this['id'] + ")'>" + this['name'] + "</a> " +
                        "<a href='#' onclick='deleteHotel(" + this['id'] + ")'>(Delete)</a>" +
                        "</li>";
            });
            $("#hotelList").html("<ol>" + hotelListHTMLBody + "</ol>");
            reloadHotelDetail(hotel[0].id);
        });

        function deleteHotel(hotelId) {
            console.log("Deleting hotelId " + hotelId);
            $.ajax({
                type: "POST",
                url: "deleteHotel/" + hotelId,
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        }

        // Fetch and show detailed location information
        function reloadHotelDetail(id) {
            console.log("Reloading hotel detail for #" + id);
            $.getJSON("../hotel/" + id, function (hotelArray) {

                var hotel = hotelArray[0];
                $("#hotelId").text(hotel.hotel_id).val(hotel.hotel_id);
                $("#hotelName").text(hotel.hotel_name).val(hotel.hotel_name);
                $("#hotelImage").text(hotel.hotel_image).val(hotel.hotel_image);
                $("#hotelPrice").text(hotel.hotel_price).val(hotel.hotel_price);
                $("#hotelDetail").text(hotel.hotel_detail).val(hotel.hotel_detail);
                $("#hotelLat").text(hotel.hotel_lat).val(hotel.hotel_lat);
                $("#hotelLng").text(hotel.hotel_lng).val(hotel.hotel_lng);
                $("#hotelAlamat").text(hotel.hotel_alamat).val(hotel.hotel_alamat);
                $("#hotelTelepon").text(hotel.hotel_telepon).val(hotel.hotel_telepon);
                $("#hotelDistance").text(hotel.hotel_distance).val(hotel.hotel_distance);
                $("#hotelWebsite").text(hotel.hotel_website).val(hotel.hotel_website);
            });
        }

        // AJAX form posting
        var form = $('#hotelForm');
        form.submit(function (ev) {

            console.log("Form sent!");
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: new FormData(form),
                success: function (data) {
                    alert(data);
                }
            });

            ev.preventDefault();
        });

        $('#addHotel').click(function (ev) {
            console.log("Add hotel clicked!");
            $.ajax({
                type: "POST",
                url: "addHotel",
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        })

    </script>
@endsection
