@extends('cms.master')

@section('content')
    <div class="col-md-3">
        <h3 class="page-header">Specialization</h3>
        <p id="specializationList">(ada empat spesialisasi)</p>
    </div>
    <div class="col-md-3">
        <h3 class="page-header" id="specName">Symposium</h3>
        <p id="symposiumList">Loading...</p>
        <button type="button" class="btn btn-primary" id="addSymposium">Add Symposium</button>
    </div>
    <div class="col-md-6">
        <h3 class="page-header">Symposium Detail</h3>
        {{ Form::open(['action' => 'CmsController@updateSymposium', 'files' => 'true', 'id' => 'workshopForm', 'class' => 'form-horizontal']) }}
        <div class="form-group">
            <label class="sr-only col-sm-2 control-label">ID</label>
        </div>

        {{-- SYMPOSIUM ID --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">ID</label>
            <div class="col-sm-10">
                <input class="form-control" id="symposiumId" name="symposiumId">
            </div>
        </div>

        {{-- SYMPOSIUM NAME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Name</label>
            <div class="col-sm-10">
                <input class="form-control" id="symposiumName" name="symposiumName">
            </div>
        </div>

        {{-- SYMPOSIUM IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Image</label>
            <div class="col-sm-10">
                <input class="form-control" id="symposiumImage" name="symposiumImage">
            </div>
        </div>

        {{-- UPLOAD IMAGE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">{!! Form::label('upload', 'Upload') !!}</label>
            <div class="col-sm-10">
                {!! Form::file('upload') !!}
            </div>
        </div>

        {{-- SYMPOSIUM DESCRIPTION --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Description</label>
            <div class="col-sm-10">
                <input class="form-control" id="symposiumDescription" name="symposiumDescription">
            </div>
        </div>

        {{-- SYMPOSIUM DATE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Date</label>
            <div class="col-sm-10">
                <input class="form-control" id="symposiumDate" name="symposiumDate">
            </div>
        </div>

        {{-- SYMPOSIUM START TIME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Start Time</label>
            <div class="col-sm-10">
                <input class="form-control" id="symposiumStartTime" name="symposiumStartTime">
            </div>
        </div>

        {{-- SYMPOSIUM END TIME --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">End Time</label>
            <div class="col-sm-10">
                <input class="form-control" id="symposiumEndTime" name="symposiumEndTime">
            </div>
        </div>

        {{-- SYMPOSIUM VENUE --}}
        <div class="form-group">
            <label class="col-sm-2 control-label">Venue</label>
            <div class="col-sm-10">
                <input class="form-control" id="symposiumVenue" name="symposiumVenue">
            </div>
        </div>

        {{-- BUTTON TO SEND THE FORM AND UPDATE SYMPOSIUM --}}
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Update Symposium Detail</button>
            </div>
        </div>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script>

        $('#symposiumSidebarItem').addClass("active");

        // Fetch and show list of specializations
        $.getJSON("../symposium/specialization", function (specializations) {
            console.log("Getting all specialization...");
            var specializationListHTMLBody = "";
            $.each(specializations, function (index, element) {
                console.log("SYMPOSIUM id: " + index + ", name " + element.name + ", eventId = " + element.id);
                specializationListHTMLBody += "<li>" +
                        "<a href='#' onclick='reloadSymposiumList(" + this['id'] + ")'>" + this['name'] + "</a>" +
                        "</li>";
            });
            $("#specializationList").html("<ol>" + specializationListHTMLBody + "</ol>");
            reloadSymposiumList(specializations[0].id); // hardcoded wkwkwkwk
        });

        // Fetch and show list of all symposiums for a given specialization
        function reloadSymposiumList(specializationId) {

            selectedSpecializationId = specializationId;

            // changing name for specName header
            console.log("changing name...");
            $.getJSON("../symposium/specialization/" + specializationId, function (specialization) {
                $("#specName").text(specialization[0].symspec_name);
            });

            $.getJSON("../symposium/getAllForSpec/" + specializationId, function (symposiums) {
                console.log("Getting all specialization for specialization " + specializationId + "...");
                var symposiumListHTMLBody = "";
                $.each(symposiums, function (index, element) {
                    console.log("SYMPOSIUM id: " + index + ", name " + element.name + ", eventId = " + element.id);
                    symposiumListHTMLBody += "<li>" +
                            "<a href='#' onclick='reloadSymposiumDetail(" + this['id'] + ")'>" + this['name'] + "</a> " +
                            "<a href='#' onclick='deleteSymposium(" + this['id'] + ")'>(Delete)</a>" +
                            "</li>";
                });
                $("#symposiumList").html("<ol>" + symposiumListHTMLBody + "</ol>");
                reloadSymposiumDetail(symposiums[0].id);
            });
        }

        function reloadSymposiumDetail(symposiumId) {

            selectedSymposiumId = symposiumId;

            console.log("Reloading SYMPOSIUM detail for #" + symposiumId);
            $.getJSON("../symposium/" + symposiumId, function (symposium) {

                console.log(symposium[0]); // get the zeroth hotel

                // iterate through JSONObject's keys
                $.each(symposium[0], function (key, value) {
                    switch (key) {
                        case "symposium_id":
                            $("#symposiumId").text(value).val(value);
                            return true;
                        case "symposium_name":
                            $("#symposiumName").text(value).val(value);
                            return true;
                        case "symposium_image":
                            $("#symposiumImage").text(value).val(value);
                            return true;
                        case "symposium_description":
                            $("#symposiumDescription").text(value).val(value);
                            return true;
                        case "symposium_date":
                            $("#symposiumDate").text(value).val(value);
                            return true;
                        case "symposium_start_time":
                            $("#symposiumStartTime").text(value).val(value);
                            return true;
                        case "symposium_end_time":
                            $("#symposiumEndTime").text(value).val(value);
                            return true;
                        case "symposium_venue":
                            $("#symposiumVenue").text(value).val(value);
                            return true;
                    }
                });
            });
        }

        function deleteSymposium(symposiumId) {
            console.log("Deleting symposiumId " + symposiumId);
            $.ajax({
                type: "POST",
                url: "deleteSymposium/" + symposiumId,
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        }

        $('#addSymposium').click(function (ev) {
            console.log("Add symposium clicked!");
            $.ajax({
                type: "POST",
                url: "addSymposiumForSpec/" + selectedSpecializationId,
                success: function (result) {
                    alert(result);
                    location.reload();
                }
            });
        });

        var form = $('#symposiumForm');
        form.submit(function (ev) {
            console.log("Form sent!");
            $.ajax({
                type: form.attr('method'),
                url: form.attr('action'),
                data: new FormData(form),
                success: function (data) {
                    alert(data);
                    reloadSymposiumList(selectedSpecializationId);
                    reloadSymposiumDetail(selectedSymposiumId);
                }
            });

            ev.preventDefault();
        });

    </script>
@endsection
